function X = RefSyEvCL(A, X)
% REFSYEVCL Iterative refinement of eigenvalues and vectors
%           targeting clustered eigenvalues
% function X = RefSyEvCL(A, X)
% Input symmetric matrix A for the eigen problem A*X=X*D.
% Input X is the initial values of eigenvectors X.
% 
% It follows the Algorithm 2 of the paper:
% https://link.springer.com/article/10.1007/s13160-019-00348-4
%
% Implemented by: Yaohung Mike Tsai  <ytsai2@vols.utk.edu>
%
n=size(A,1);
[X, D, E, delta] = RefSyEv(A, X);
[X, D, E, delta] = RefSyEv(A, X);
[X, D, E, delta] = RefSyEv(A, X);
if norm(E) >= 1 % improve cannot be expected
    return
end

% sort and cluster the eigenvalues
d=diag(D);
[sorted_d, index_d] = sort(d);
i=1;
cluster_id=1;
cluster_map=zeros(n,1);
while(i<=n)
    cluster_map(index_d(i))=cluster_id;
    i=i+1;
    while(i<=n)
        if(sorted_d(i)-sorted_d(i-1)<delta)
            cluster_map(index_d(i))=cluster_id;
        else
            break
        end
        i=i+1;
    end
    cluster_id=cluster_id+1;
end

% refine for each cluster
for k=1:cluster_id-1
    V_k=X(:,cluster_map==k);
    if(size(V_k,2)==1)
        continue
    end
    dc=d(cluster_map==k);
    mu_k=(max(dc)+min(dc))/2; % calculate the shift constant
    A_k=A-mu_k*eye(n);        % shift A
    C_k=V_k'*A_k*V_k;
    [W_k D_k]=eig(single(C_k)); % solve small eigensystem in low precision
    V_k=V_k*double(W_k);      % update V_k
    while(1)
        [V_k, D_k, E_k, delta_k] = RefSyEv(A_k, V_k);
        if(norm(E_k)>=1)      % improve cannot be expected
            return
        end
        if(norm(E_k)<=norm(E))
            break
        end
    end
    X(:,cluster_map==k)=V_k; % store the updated eigenvectors
end
    
    