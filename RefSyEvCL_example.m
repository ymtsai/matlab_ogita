n=1000;
A=rand(n);
A=(A+A')/2;
%A=wilkinson(n);
%A=gallery('randsvd',n,-1/eps,1);
% last argument can be 1-5
% 1: One large singular value.
% 2: One small singular value.
% 3: Geometrically distributed singular values (default).
% 4: Arithmetically distributed singular values.
% 5: Random singular values with uniformly distributed logarithm.
[X,D] = eig(single(A));
X=double(X);
D=double(D);
disp(['Norm of residual A*X-X*D at beginning      ' num2str(norm(A*X-X*D))]);
[X_ref,D_ref] = eig(A);
d_ref=diag(D_ref);


X = RefSyEvCL(A, X);
D = diag(diag(X'*A*X));
disp(['Norm of residual A*X-X*D after RefSyEvCL  ' num2str(norm(A*X-X*D))]);

% X = RefSyEvCL(A, X);
% D = diag(diag(X'*A*X));
% disp(['Norm of residual A*X-X*D after RefSyEvCL  ' num2str(norm(A*X-X*D))]);
% 
% X = RefSyEvCL(A, X);
% D = diag(diag(X'*A*X));
% disp(['Norm of residual A*X-X*D after RefSyEvCL  ' num2str(norm(A*X-X*D))]);

